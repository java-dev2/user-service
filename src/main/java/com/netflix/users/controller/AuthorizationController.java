package com.netflix.users.controller;

import com.netflix.common.utils.Constants;
import com.netflix.users.dtos.LoginDto;
import com.netflix.common.dto.AuthorizationDto;
import com.netflix.users.service.AuthorizationService;
import com.netflix.users.utils.TokenProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/v1")
public class AuthorizationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenProvider tokenProvider;
    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private HttpServletRequest httpServletRequest;

    @PostMapping(value = "/authenticate")
    public ResponseEntity<LoginDto> authenticate(@RequestBody LoginDto loginDto) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(loginDto.getUserName(), loginDto.getPassword());
        Authentication authenticate = authenticationManager.authenticate(authentication);
        String token = Constants.BEARER_PREFIX.concat(tokenProvider.generateToken(authenticate.getName()));
        LoginDto response = new LoginDto();
        response.setTokenId(token);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping(value = "/authorization")
    public ResponseEntity<AuthorizationDto> checkApiPermission(@RequestParam("url") String url, @RequestParam("method") String method) {
        int index = url.indexOf("/api/");
        url = url.substring(index);
        String tokenId = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.startsWith(tokenId, "Bearer ")) {
            tokenId = tokenId.substring(7);
        }
        AuthorizationDto authorizationDto = authorizationService.checkApiPermission(url, method, tokenId);
        return ResponseEntity.ok(authorizationDto);
    }
}
