package com.netflix.users.repository;

import com.netflix.users.entities.ApiPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApiPermissionRepository extends JpaRepository<ApiPermission, Long> {
    @Query("SELECT apiPermission FROM ApiPermission AS apiPermission WHERE apiPermission.apiMethod = :method " +
            "AND apiPermission.isDeleted = :isDeleted " +
            "AND EXISTS (SELECT 1 FROM UserRole AS userRole JOIN userRole.role AS role " +
            "JOIN role.rolePermissions rolePermissions WHERE userRole.userRoleEmbeddable.userId = :userId " +
            "AND role.isDeleted = :isDeleted AND rolePermissions.rolePermissionEmbeddable.apiPermissionId = apiPermission.apiPermissionId)")
    List<ApiPermission> getApiPermissionByUserDid(@Param("userId") Long userDid,
                                                         @Param("method") String method,
                                                         @Param("isDeleted") boolean isDeleted);
}
