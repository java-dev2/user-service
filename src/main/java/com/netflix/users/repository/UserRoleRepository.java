package com.netflix.users.repository;

import com.netflix.users.entities.UserRole;
import com.netflix.users.entities.UserRoleEmbeddable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleEmbeddable> {
    @Query("SELECT userRole FROM UserRole userRole INNER JOIN UserInformation userInformation " +
            "ON userRole.userInformation.userId = userInformation.userId " +
            "INNER JOIN Role role ON role.roleId = userRole.role.roleId " +
            "WHERE userRole.userInformation.userId = :userId " +
            "AND role.isDeleted = :isDeleted " +
            "AND userInformation.isDeleted = :isDeleted")
    List<UserRole> findUserRoleByUserId(@Param("userId") Long userId,
                                        @Param("isDeleted") boolean isDeleted);
}
