package com.netflix.users.service;

import com.netflix.common.dto.AuthorizationDto;

public interface AuthorizationService {
    AuthorizationDto checkApiPermission(String url, String method, String tokenId);
}
