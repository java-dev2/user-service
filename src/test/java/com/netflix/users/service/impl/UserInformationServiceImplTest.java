package com.netflix.users.service.impl;

import com.netflix.common.constants.Gender;
import com.netflix.users.dtos.UserRequest;
import com.netflix.users.dtos.response.UserResponse;
import com.netflix.users.entities.Role;
import com.netflix.users.entities.UserInformation;
import com.netflix.users.entities.UserRole;
import com.netflix.users.repository.RoleRepository;
import com.netflix.users.repository.UserInformationRepository;
import com.netflix.users.repository.UserRoleRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UserInformationServiceImplTest {

    @Mock
    private UserInformationRepository userInformationRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private UserRoleRepository userRoleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @InjectMocks
    private UserInformationServiceImpl userInformationService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadUserByUsernameSuccess() {
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(1L);
        userInformation.setPassword("password");
        when(userInformationRepository.findUserInformationByUsername(eq("userName"), eq(false))).thenReturn(Optional.of(userInformation));
        UserDetails userDetails = userInformationService.loadUserByUsername("userName");
        assertNotNull(userDetails);
        assertEquals(userDetails.getUsername(), "userName");
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameFailed() {
        when(userInformationRepository.findUserInformationByUsername(eq("userName"), eq(false))).thenReturn(Optional.empty());
        userInformationService.loadUserByUsername("userName");
    }

    @Test
    public void createUser() {
        UserRequest userRequest = new UserRequest();
        userRequest.setUserName("userName");
        userRequest.setPassword("password");
        userRequest.setDateOfBirth("1997-07-13");
        userRequest.setGender('M');
        userRequest.setFirstName("Tung");
        userRequest.setLastName("Nguyen");
        Role role = new Role();
        role.setRoleId(1L);
        role.setRoleType("EMPLOYEE");
        when(roleRepository.findRoleByRoleType(eq("EMPLOYEE"))).thenReturn(Optional.of(role));
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(1L);
        userInformation.setUsername("userName");
        userInformation.setPassword("password");
        userInformation.setDateOfBirth(LocalDate.now());
        userInformation.setGender(Gender.MALE);
        when(passwordEncoder.encode(eq(userRequest.getPassword()))).thenReturn("hashPassword");
        when(userInformationRepository.save(any(UserInformation.class))).thenReturn(userInformation);
        when(userRoleRepository.save(any(UserRole.class))).thenReturn(new UserRole());
        UserResponse employee = userInformationService.createUser(userRequest, "EMPLOYEE");
        assertEquals(employee.getUsername(), "userName");
    }

    @Test
    public void getUserLoginProfile() {
        UserInformation userInformation = new UserInformation();
        userInformation.setUserId(1L);
        userInformation.setUsername("userName");
        userInformation.setPassword("password");
        userInformation.setDateOfBirth(LocalDate.now());
        userInformation.setGender(Gender.MALE);
        when(userInformationRepository.findUserInformationByUsername(anyString(), anyBoolean())).thenReturn(Optional.of(userInformation));
        UserRole userRole = new UserRole();
        Role role = new Role();
        role.setRoleType("EMPLOYEE");
        userRole.setRole(role);
        when(userRoleRepository.findUserRoleByUserId(anyLong(), anyBoolean())).thenReturn(Collections.singletonList(userRole));
        UserResponse userResponse = userInformationService.getUserLoginProfile("userName");
        assertNotNull(userResponse);
    }
}